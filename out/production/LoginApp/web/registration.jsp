<%--
  Created by IntelliJ IDEA.
  User: alinaharutyunyan
  Date: 2019-07-16
  Time: 03:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form action="registration">
    <div class="container">
        <h1>Register</h1>

        <p>Please fill in this form to create an account.</p>

        <hr>

        <input type="text" placeholder="Enter FirstName" name="firstName">

        <input type="text" placeholder="Enter LastName" name="lastName">

        <input type="email" placeholder="Enter email" name="email">

        <input type="password" placeholder="Enter password" name="password">

        <hr>

        <button type="submit" class="register">Register</button>

    </div>

    </div>
</form>
</body>
</html>
