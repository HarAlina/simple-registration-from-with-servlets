package model.validation;

public class RegistrationValidator {

    public boolean isValid(String firstName, String lastName, String email, String password) {
        return !firstName.equals("") && !lastName.equals("") && !email.equals("") && !password.equals("");
    }

}
