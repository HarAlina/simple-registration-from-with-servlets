package model.impl;

import model.User;
import model.interfaces.Authenticator;
import model.interfaces.UserManager;



public class AuthenticatiorImpl implements Authenticator {


    private UserManager userManager;

    @Override
    public boolean isAuthenticated(String email, String password) {
        boolean isAuthenticated = false;

        for (User user : userManager.findAll()) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                isAuthenticated = true;
                break;
            }
        }
        return isAuthenticated;
    }

    @Override
    public boolean isActive(UserManager userManager) {
        return false;
    }
}
