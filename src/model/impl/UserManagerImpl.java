package model.impl;

import model.User;
import model.interfaces.UserManager;
import java.util.ArrayList;
import java.util.List;

public class UserManagerImpl implements UserManager {

    private static UserManagerImpl instance;

    private UserManagerImpl() {

    }

    public static UserManagerImpl getInstance() {
        if (instance == null) {
            instance = new UserManagerImpl();
        }
        return instance;
    }

    private List<User> users = new ArrayList<>();

    @Override
    public User save(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User update(User user) {
        int id = (int) user.getId();
        users.set(id, user);
        return user;
    }

    @Override
    public void delete(int id) {
        users.remove(id);
    }

    @Override
    public User findById(int id) {
        return users.get(id);
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findByEmail(String email) {
        for (User user: users) {
            if (user.getEmail().equals(email)){
                return user;
            }
        }
        return null;
    }
}
