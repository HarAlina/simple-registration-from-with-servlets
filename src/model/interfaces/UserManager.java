package model.interfaces;

import model.User;

import java.util.List;

public interface UserManager {

    User save(User user);

    User update(User user);

    void delete(int id);

    User findById(int id);

    List<User> findAll();

    User findByEmail(String email);
}