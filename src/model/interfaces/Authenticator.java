package model.interfaces;

public interface Authenticator {

    boolean isAuthenticated(String email, String password);

    boolean isActive(UserManager user);
}
