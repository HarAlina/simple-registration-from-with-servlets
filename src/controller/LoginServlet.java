package controller;



import model.User;
import model.impl.UserManagerImpl;
import model.interfaces.Authenticator;
import model.interfaces.UserManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {


    private Authenticator authenticator;

    UserManager userManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (authenticator.isAuthenticated(email, password)) {
            HttpSession session = req.getSession(false);
            userManager = UserManagerImpl.getInstance();
            User user = userManager.findByEmail(email);
            //session.setAttribute(..);

            resp.getWriter().println("Welcome " + userManager.findByEmail(email).getFirstName());
            req.getRequestDispatcher("/welcome.jsp").include(req, resp);
        }else {
            resp.getWriter().println("Wrong credentials");
            req.getRequestDispatcher("/login.jsp").include(req,resp);
        }

    }
}
