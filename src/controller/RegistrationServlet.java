package controller;

import model.User;
import model.impl.UserManagerImpl;
import model.interfaces.UserManager;
import model.validation.RegistrationValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class RegistrationServlet extends HttpServlet {


    private UserManager userManager;
    private RegistrationValidator validator;

    @Override
    public void init() throws ServletException {
        this.validator = new RegistrationValidator();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        RequestDispatcher requestDispatcherIndex = req.getRequestDispatcher("/index.jsp");
        RequestDispatcher requestDispatcherRegistration = req.getRequestDispatcher("/registration.jsp");

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (validator.isValid(firstName,lastName,email,password)) {
            userManager= UserManagerImpl.getInstance();
            userManager.save(new User(firstName, lastName, email, password));
            resp.getWriter().println("You are successfully registered ! ");
            requestDispatcherIndex.include(req,resp);
        }else {
            resp.getWriter().println("Please fill in all fields");
            requestDispatcherRegistration.include(req,resp);
        }

    }
}
